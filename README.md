# Node_Server #
---

## Info
---

Git Link: [https://bitbucket.org/dev909/node_server](https://bitbucket.org/dev909/node_server)  


This website uses a login system. Depending on the currently logged in user, pages behave differently. The website also has a "topbar", a bar that is displayed on the top of the screen that provides info and navigation for the currently logged in user. This topbar allows users to visit their profile, logout, view all the registered users, view their location, and view their profile picture. The website allows users to add and remove friends by visiting their profile. Users can also visit the location pages of each person. The location page will display the user that owns the location page, as well as their friends locations in both a google map and a table. The map provides markers that displays the 5 most recent locations for that user. The table displays the latest locations for each user, with the most recent locations towards the top of the table.     

Much of the code is dirty and messy. The focus was to get the code working first. While some things could have been improved that could have lead to easier code, we were most likely too far in development and under time constraints to be able to improve them. In many cases there is duplicate code, full array transfers, bad naming conventions, and hoops to jump through. Additionally some logic (that due to the bad infrastructure), is not ideal (such as defaulting to the first user if there is no logged in user detected). Its messy, but it works.   


## API
--- 

===========================
### Objects
===========================

**Name:** user  
**Format** JSON  
**Fields:**  
    - (int) id: The id of the user. All server operations uses this.  
    - (String) name: The name of the user. Used to login.  
    - (String) pass: The password of the user. Used to login.
    - (String image: the image resource of the user.  
    - (String) birthday: The birthday of the user.  
    - (String) address: The address of the user.  
    - (String) phone: The phone number of the user.  
    - (int[]) friends: The array that contains the id's of all of the current user's friends.  
    - (Location[]) location: The array that contains the 5 most recent locations of the user.  
**Description:**  
The main user object, that contains everything needed for a user.
---
**Name:** location  
**Format** JSON  
**Fields:**  
	- (String) loc: The location name.
    - (String) lat: The latitude of the location.  
    - (String) long: The longitude of the location. 
    - (String) date: The date of the time the location was created. 
**Description:**  
The location object which contains the latitude and longitude.

===========================
### Methods 
===========================

**Name:** /api/users  
**Method:** GET  
**Params:**  
    - id: The id of the specific user  
**Description:**  
If given an id, then returns the user object.  
If not given an id, then returns the entire user database.  
---
**Name:** /api/addFriend  
**Method:** POST  
**Params:**  
    - id: The currently logged in user (the user requesting the friend)  
    - friendId: The friend's id  
**Description:**  
Adds the friend's id to the current user's friends array. Checks to make sure the two users are not friends already.  
---
**Name:** /api/removeFriend  
**Method:** POST  
**Params:**  
    - id: The currently logged in user (the user requesting the friend)  
    - friendId: The friend's id  
**Description:**  
Removes the friend's id of the current user's friends array. Checks to make sure the two users are friends first.  
---
**Name:** /api/location  
**Method:** POST  
**Params:**  
    - id: The currently logged in user  
    - loc: The index of the location
    - date: The date the location was created
    - trueLat: The true latitude of the current user  
    - trueLong: The true longitude of the current user  
**Description:**  
Adds the current location to the user's location array. Also keeps the location array to a max length of 5, with the newest location in the last index.  


### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.