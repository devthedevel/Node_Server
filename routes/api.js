var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require("fs");

//File path
var fileName = "data/users.json";

/*
 * GET /users
 * Params:
 * 	- id: The id of the requested user
 * Returns the user object of the specific id. If given no id, then returns the entire users array
 */
router.get('/users', function(req, res, next) {
	//Read file async
	fs.readFile(fileName, function(err, data) {
		//Retrieve complete list of users
		var users = readData(err, data, req, res);
		//If specific user id is given, return specific user
		if (req.query.id != null) {
			res.json(users.users[req.query.id-1]);
		}
		//Else return entire userbase
		else {
			return res.json(users);
		}
	});
});

/*
 * POST /addFriend
 * Params:
 * 	- id: The id of the requested user
 *  - friendId: The id of the requested friend
 * Adds the friend user to the requested user's friend array. Will not add friends if they are already friends
 */
router.post('/addFriend', function(req, res, next) {
	//Read file async
	fs.readFile(fileName, function(err, data) {
		//Declare and initialize variables
		var cId = req.body.id;
		var fId = req.body.friendId;
		//Read the entire users array
		var users = readData(err, data, req, res);
		var currentUser = users.users[cId-1];
		//Check to see if current user is requesting to add itself
		if (cId == fId) {
			res.redirect("../profile?id=" + cId);
			return;
		}
		//If not, then add the friend user to the current user's friends array if friends array is undefined
		else if (currentUser.friends.length == 0 || currentUser.friends == null) {
			currentUser.friends.push(fId);
			users.users[cId-1] = currentUser;
		}
		//If not, then add the friend user to the current user's friends array
		else {
			var flag = false;
			for (var i = 0; i < currentUser.friends.length; i++) {
				//Check to see if the two user's are friends already
				if (currentUser.friends[i] == fId) {
					flag = true;
				}
			}
			if (!flag) {
				currentUser.friends.push(fId);
				users.users[cId-1] = currentUser;
			}
		}
		//Write the entire user array to server storage
		fs.writeFile(fileName, JSON.stringify(users), function(err) {
			if (err) throw err;
			res.redirect("../profile?id=" + cId);		
		});		
	});
});

/*
 * POST /removeFriend
 * Params:
 * 	- id: The id of the requested user
 *  - friendId: The id of the requested friend
 * Removes the friend user from the requested user's friend array
 */
router.post('/removeFriend', function(req, res, next) {
	//Read file async
	fs.readFile(fileName, function(err, data) {
		//Declare and initialize variables
		var cId = req.body.id;
		var fId = req.body.friendId;
		var users = readData(err, data, req, res);
		var currentUser = users.users[cId-1];
		//Check to see if current user already has specific friend
		if (currentUser.friends.length == 0 || currentUser.friends == null) {
			return;
		}
		else {
			//Remove friend
			for (var i = 0; i < currentUser.friends.length; i++) {
				if (currentUser.friends[i] == fId) {
					currentUser.friends.splice(i, 1);
				}
			}
		}
		users.users[cId-1] = currentUser;
		//Write the entire user array back to server storage
		fs.writeFile(fileName, JSON.stringify(users), function(err) {
			if (err) throw err;
			res.redirect("../profile?id=" + cId);		
		});	
	});
});

/*
 * POST /location
 * Params:
 * 	- id: The id of the requested user
 *  - loc: The index of the location
 *  - date: The date that the location was updated
 *  - trueLat: The true latitude of the user
 *  - trueLong: The true longitude of the user
 * Adds the location to the user's location array. 
 * Automatically keeps array limited to the 5 most recent locations, with the most recent towards the end of the array
 */
router.post('/location', function(req, res, next) {
	//Declare and initialize locations array
	var locations = [
		{name: "Avalon Mall",
			lat: 47.561373, long: -52.753551},
		{name: "Downtown",
				lat: 47.535275, long: -52.750007},
		{name: "Village Mall",
				lat: 47.561507, long: -52.710268},
		{name: "MUN UC",
				lat: 47.573465, long: -52.735384},
		{name: "True",
				lat: 0.0, long: 0.0}
	];
	//Read file async
	fs.readFile(fileName, function(err, data) {
		//Declare and initialize variables
		var MAX = 5;
		var id = req.body.id;
		var locIndex = req.body.loc;
		var date = req.body.date;
		var users = readData(err, data, req, res);
		var currentUser = users.users[id-1];
		var loc = locations[locIndex].name;
		var lat = locations[locIndex].lat;
		var long = locations[locIndex].long;
		//If user selected true location, then set latlong to true coords
		if (locIndex == 4) {
			lat = req.body.trueLat;
			long = req.body.trueLong;
		}
		//Add location to user's location array if the location array is less than the max
		if (currentUser.location.length < MAX) {
			currentUser.location.push({"loc": loc, "lat": lat, "long": long, "date": date});
		}
		//Shift locations down one index, and then add location to the user's location array
		else {
			for (var i = 0; i < currentUser.location.length-1; i++) {
				currentUser.location[i] = currentUser.location[i+1];
			}
			currentUser.location[currentUser.location.length-1] = {"loc": loc, "lat": lat, "long": long, "date": date};
		}
		users.users[id-1] = currentUser;
		//Write entire user array back to storage
		fs.writeFile(fileName, JSON.stringify(users), function(err) {
			if (err) throw err;
			res.redirect("../location?id=" + id);		
		});	
	});
});

/* Returns the complete list of registered users */
function readData(err, data, req, res) {
	var users;
	if (err) {
		console.log("Error during file reading");
		console.log(err);
	}
	else {
		users = JSON.parse(data);
	}
	console.log("File read successfully");
	//Check to make sure users are not null
	if (users == null) {
		users = {"users":[]};
	}
	return users;
}

module.exports = router;
