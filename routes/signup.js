var express = require('express');
var router = express.Router();

//File path
var fileName = "data/users.json";

/*
 * GET /signup
 * Displays simple signup page
 */
router.get('/', function(req, res, next) {
	res.render('signup', {title: "Greatest Social Network", action: "Sign-Up", actionUrl: "/signup"});
});

module.exports = router;
