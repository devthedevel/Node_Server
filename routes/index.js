var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require("fs");

//File path
var fileName = "data/users.json";

/*
 * GET /
 * Main index page. If no one is currently logged in then go to login page.
 * If someone is logged in, then go to their profile
 */
router.get('/', function(req, res, next) {
	//Check if theres a logged in user
	if (req.session.loggedIn) {
		res.redirect('../profile');
	}
	else {
		res.render('index', { title: "Greatest Social Network", action: "Login", actionUrl: "/"});
	}
});

/*
 * GET /profilePicture
 * Display a page with the fullsize profile picture
 */
router.get('/profilePicture', function(req, res, next) {
	//Check if theres a logged in user
	if (req.session.loggedIn) {
		var user = JSON.parse(req.session.loggedIn);
		res.render('profilePicture', {name: user.name});
	}
	else {
		res.send("Logged in user not available");
	}
});

/*
 * GET /people
 * Displays a page of all currently registered users on the site. Useful for navigating to other users
 */
router.get('/people', function(req, res, next) {
	//Read all the users
	fs.readFile(fileName, function(err, data) {
		var currentUser;
		var users = readData(err, data, req, res);
		//Check to see if theres a logged in user
		if (req.session.loggedIn) {
			currentUser = JSON.parse(req.session.loggedIn);
		}
		//If not then default to first user
		else {
			currentUser = users.users[0];
		}
		res.render('people',{title: "Greatest Social Network", action: "People", currentUser: currentUser.name, currentId: currentUser.id, currentPic: currentUser.image});
	});
});

/*
 * GET /logout
 * Display a page for the user logging out. Deletes session data
 */
router.get('/logout', function(req, res, next) {
	//Check to see if theres a logged in user
	if (req.session.loggedIn) {
		delete req.session.loggedIn;
	}
	res.render('logout', { title: "Greatest Social Network", action: "Logged Out Successfully", actionUrl: "/"});
});

/*
 * GET /location
 * Displays a location page that shows the user and their friends locations
 */
router.get('/location', function(req, res, next) {
	//Read all users
	fs.readFile(fileName, function(err, data) {
		var users = readData(err, data, req, res);
		var currentUser;
		//Check if theres a logged in user
		if (req.session.loggedIn) {
			currentUser = JSON.parse(req.session.loggedIn);
		}
		//Default to first user if not
		else {
			currentUser = users.users[req.query.id-1];
		}
		var profileUser = users.users[req.query.id-1];
		var hide = "hidden";
		//If looking at the logged in user's location, then unhide the location form
		if (currentUser.id == profileUser.id) {
			hide = "";
		}
		res.render('location', {
			currentUser: currentUser.name,
			profileUser: profileUser.name,
			currentId: currentUser.id,
			profileId: profileUser.id,
			currentPic: currentUser.image,
			hide: hide});
	})
});

/*
 * POST /signup
 * Displays a page to allow users to signup to the website.
 */
router.post('/signup', function(req, res, next) {
	//Read all users
	fs.readFile(fileName, function(err, data) {
		var users = readData(err, data, req, res);
		var user = {id: "",
					name: "",
					pass: "",
					image: "userThumb",
					birthday: "Test date",
					address: "Test address",
					phone: "Test number",
					friends: [],
					location: []
		};
		user.id = users.users.length + 1;
		user.name = req.body.name;
		user.pass = req.body.pass;
		users.users.push(user);
		//Write all users to server storage
		fs.writeFile(fileName, JSON.stringify(users), function(err) {
			if (err) throw err;
			res.render('signupSuccessful', {title: "Greatest Social Network", action: "Sign-up Successful!"});
		});	
	});
});

/*
 * POST /
 * Handles logging in to the website. If login failed then display a loginFailed page.
 * If login is successful, then display the logged in user's profile page
 */
router.post('/', function(req, res, next) {
	//Read file async
	fs.readFile(fileName, function(err, data) {
		var users = readData(err, data, req, res);
		var flag = false;
		var user;
		//Check to see if the login credentials are valid
		for (var i = 0; i < users.users.length; i++) {
			if (users.users[i].name == req.body.name && users.users[i].pass == req.body.pass) {
				user = users.users[i];
				flag = true;
				break;
			}
		}
		//If valid, the display profile page
		if (flag) {
			req.session.loggedIn = JSON.stringify(user);
			res.redirect('../profile?id=' + user.id);
		}
		//If not, then displayed login failed page
		else {
			res.render('loginFailed', {title: "Greatest Social Network", action: "Login Failed"});
		}
	});
});

/* Returns the complete list of registered users */
function readData(err, data, req, res) {
	var users;
	if (err) {
		console.log("Error during file reading");
		console.log(err);
	}
	else {
		users = JSON.parse(data);
	}
	console.log("File read successfully");
	//Check to make sure users are not null
	if (users == null) {
		users = {"users":[]};
	}
	return users;
}

module.exports = router;
