var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require("fs");

//File path
var fileName = "data/users.json";

/*
 * GET /
 * Handles profile page.
 */
router.get('/', function(req, res, next) {
	//Read file async
	fs.readFile(fileName, function(err, data) {
		var users = readData(err, data, req, res);
		var profileUser = users.users[req.query.id-1];
		var currentUser;
		//Check if someones logged in
		if (req.session.loggedIn) {
			currentUser = JSON.parse(req.session.loggedIn);
		}
		//If not then default to first user
		else {
			currentUser = users.users[0];
		}
		//Set the labels and actions for adding/removing friends
		var friendButton = "addFriend";
		var friendButtonLabel = "Add Friend";
		var hide = "";
		//If the profile user is the logged in user, then hide add/remove friend button
		if (currentUser.id == profileUser.id) {
			hide = "hidden";
		}
		//If not, then check to see if the two user's are friends.
		//If they are, then change logic to remove friend
		if (isFriends(currentUser, profileUser)) {
			friendButton = "removeFriend";
			friendButtonLabel = "Remove Friend";
		}
		//Render profile page with correct data
		res.render('profile', {
			currentUser: currentUser.name, 
			profileUser: profileUser.name, 
			birthday: profileUser.birthday, 
			address: profileUser.address, 
			phone: profileUser.phone, 
			currentId: currentUser.id, 
			profileId: profileUser.id,
			currentPic: currentUser.image,
			profilePic: profileUser.image,
			friendButton: friendButton,
			friendButtonLabel: friendButtonLabel,
			hide: hide});
	});
});

//Detemines if two users are friends or not
function isFriends(user, friend) {
	var isFriend = false;
	for (var i = 0; i < user.friends.length; i++) {
		if (user.friends[i] == friend.id) {
			isFriend = true;
		}
	}
	return isFriend;
}

/* Returns the complete list of registered users */
function readData(err, data, req, res) {
	var users;
	if (err) {
		console.log("Error during file reading");
		console.log(err);
	}
	else {
		users = JSON.parse(data);
	}
	console.log("File read successfully");
	//Check to make sure users are not null
	if (users == null) {
		users = {"users":[]};
		console.log("[API][GET] users were null");
	}
	return users;
}

module.exports = router;