//Declare global variables
var map;
var mapUsers;
var defaulted = false;
var locations = [
	{name: "Avalon Mall",
		lat: 47.561373, long: -52.753551},
	{name: "Downtown",
			lat: 47.535275, long: -52.750007},
	{name: "Village Mall",
			lat: 47.561507, long: -52.710268},
	{name: "MUN UC",
			lat: 47.573465, long: -52.735384},
	{name: "True",
			lat: 0.0, long: 0.0}
];
var colors = ["FF3333", "3399FF", "99FF33", "FF9933", "9933FF", "3333FF", "33FF33", "FFFF33", "33FFFF"];

window.onload = init;

function init() {
	//Check if geolocation is allowed
	if (navigator.geolocation) {
		mapUsers = getUsers();
		navigator.geolocation.getCurrentPosition(displayLoc);
	}
	else {
		alert("no geo");
	}
	
	//Initialize table
	initTable();
}

function displayLoc(pos) {
	var lat = pos.coords.latitude;
	var long = pos.coords.longitude;
	
	//Handle true latlong
	var trueLat = document.getElementById("trueLat");
	var trueLong = document.getElementById("trueLong");	
	trueLat.value = lat;
	trueLong.value = long;
	
	//Get current date
	var date = document.getElementById("date");
	date.value = new Date().toLocaleString();
	
	var profileUser;
	var profileId = document.getElementById("profileId").innerHTML;
	var req = new XMLHttpRequest();
	//GET all users
	req.open('GET', "/api/users?id=" + profileId);
	req.onload = function() {
		profileUser = JSON.parse(req.responseText);
		var div = document.getElementById("location");
		//Get the most recent location and display in text
		if (profileUser.location[profileUser.location.length-1] != undefined) {
			lat = profileUser.location[profileUser.location.length-1].lat;
			long = profileUser.location[profileUser.location.length-1].long;
			var time = profileUser.location[profileUser.location.length-1].date;
			div.innerHTML = profileUser.name + " latest location is Lat: "+ lat + " Long: " + long + " at " + time;
		}
		//Defaults to first user to prevent some crashes
		else {
			lat = locations[0].lat;
			long = locations[0].long;
			defaulted = true;
		}
		showMap(lat, long);
	}
	req.send(null);
}

function showMap(lat, long) {
	//Get latlng on user's location
	var gLatLng = new google.maps.LatLng(lat, long);
	
	var mapOptions = {
			zoom: 10,
			center: gLatLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	var mapDiv = document.getElementById("map");
	mapDiv = new google.maps.Map(mapDiv, mapOptions);
	if (!defaulted) {
		//For every eligible user (the user and friends with atleast one location) add a marker
		for (var i = 0; i < mapUsers.length; i++) {
			if (mapUsers[i].location[mapUsers[i].location.length-1] == undefined) {
				continue;
			}
			else {
				var location = mapUsers[i].location[mapUsers[i].location.length-1];
				var lat = location.lat;
				var lng = location.long;
				var time = location.date;
				var latlng = new google.maps.LatLng(lat, lng);
				addMarker(mapDiv, latlng, mapUsers[i].name, generateMarkerContent(i), i % colors.length);	
			}
		}
	}
}

//Generates the 5 most recent locations of the user. 
function generateMarkerContent(num) {
	var text = "";
	var image = "<a href=\"profilePicture\"><img width=\"50\" height=\"50\" src=\"images/" + mapUsers[num].image + ".jpg\"></a>";
	text += image;
	var name = "<h3><a href=\"/profile?id=" + mapUsers[num].id + "\">" + mapUsers[num].name + "</a></h3>";
	text += name;
	for (var i = mapUsers[num].location.length-1; i > 0; i--) {
		var location = "<em><b>@" + mapUsers[num].location[i].loc + "</b></em><br>";
		var info = "	-Time: " + mapUsers[num].location[i].date + "<br>" +
				   "	-Latitude: " + mapUsers[num].location[i].lat + "<br>" +
				   "	-Longitude: " + mapUsers[num].location[i].long + "<br>";
		text += location + info;
	}
	return text;
}

//Gets all the users
function getUsers() {
	var mapUsers = [];
	var req = new XMLHttpRequest();
	req.open('GET', "/api/users");
	req.onload = function() {
		var users = JSON.parse(req.responseText);
		var profileUser = users.users[document.getElementById("profileId").innerHTML-1];
		if (profileUser.location[profileUser.location.length-1] != undefined) {
			mapUsers.push(profileUser);
		}
		for (var i = 0; i < profileUser.friends.length; i++) {
			mapUsers.push(users.users[profileUser.friends[i]-1]);
		}
	}
	req.send(null);
	return mapUsers;
}

function addMarker(map, latlng, title, content, num) {
	//Define a custom marker to allow coloring
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + colors[num],
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
    
	var markerOptions = {
			position: latlng,
			map: map,
			icon: pinImage,
			title: title,
			clickable: true
	};
	
	var marker = new google.maps.Marker(markerOptions);
	var infoOptions = {
		content: content,
		position: latlng
	};
	var infoWindow = new google.maps.InfoWindow(infoOptions);
	google.maps.event.addListener(marker, "click", function() {
		infoWindow.open(map);
	});
}

function initTable() {
	var users;
	var req = new XMLHttpRequest();
	req.open('GET', "/api/users");
	req.onload = function() {
		//Declare and initialize variables
		users = JSON.parse(req.responseText);
		var userOrder = [];
		var profileId = document.getElementById("profileId").innerHTML;
		var body = document.getElementsByTagName("body")[0];
		var table = document.getElementById("table");
		var tBody = document.createElement("tbody");
		var tHeads = ["User", "Location", "Latitude", "Longitude", "Time"];
		var rowHeads = document.createElement("tr");
		
		//Generate table headers
		for (var i = 0; i < tHeads.length; i++) {
			var cell = document.createElement("th");
			var cellText = document.createTextNode(tHeads[i]);	
			cell.appendChild(cellText);
			rowHeads.appendChild(cell);
		}
		//Add rowHeads to tBody
		tBody.appendChild(rowHeads);
		
		//Sort users
		userOrder = sortUserOrder(users, profileId);
		
		//Add data to table
		for (var i = 0; i < userOrder.length; i++) {
			//Generate row data
			var locIndex = userOrder[i].location.length-1;
			var tableData = [userOrder[i].name,
				userOrder[i].location[locIndex].loc,
				userOrder[i].location[locIndex].lat,
				userOrder[i].location[locIndex].long,
				userOrder[i].location[locIndex].date];
			var row = document.createElement("tr");
			for (var j = 0; j < tableData.length; j++) {
				var cell = document.createElement("td");
				var cellData = document.createTextNode(tableData[j]);
				cell.append(cellData);
				row.appendChild(cell);
			}
			tBody.appendChild(row);
			
		}
		
		table.appendChild(tBody);
		body.appendChild(table);
	};
	req.send(null);
}

function sortUserOrder(users, profileId) {
	var friends = [];
	var user = users.users[profileId-1];
	if (user.location[user.location.length-1] != undefined) {
		friends.push(user);
	}
	//Get significant friends
	for (var i = 0; i < user.friends.length; i++) {
		var friend = users.users[user.friends[i]-1];
		if (friend.location[friend.location.length-1] != undefined) {
			friends.push(friend);
		}
	}
	//Sort them using bubble sort because easy to do
    var swapped;
    do {
        swapped = false;
        for (var i=0; i < friends.length-1; i++) {
        	var d1 = new Date(friends[i].location[friends[i].location.length-1].date);
        	var d2 = new Date(friends[i+1].location[friends[i+1].location.length-1].date);
            if (d1 > d2) {
                var temp = friends[i];
                friends[i] = friends[i+1];
                friends[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
    //Reverse order so most recent is first
    friends.reverse();
    
    return friends;
}